﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel;

namespace ZipMan
{
    static class Program
    {
        #region MainLoop

        [STAThread]
        static void Main(string[] arguments)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Initialize a new background work for checking updates in background
            BackgroundWorker UpdateWorker;
            UpdateWorker = new BackgroundWorker();
            UpdateWorker.DoWork += UpdateWorker_DoWork;
            UpdateWorker.RunWorkerAsync();

            // Act according to the arguments sent by the user
            if (arguments.Length >= 1)
            {
                // Get the first argument
                switch (arguments[0])
                {
                    case "--extract":
                        // Check if the archive path has been passed as an argument
                        if (arguments.Length == 2)
                        {
                            // [TODO]: Start the extraction form and pass the required args
                            Extraction.ExtractionUI ExtractWindow;
                            ExtractWindow = new Extraction.ExtractionUI(arguments[1]);
                            Application.Run(ExtractWindow);
                        } else
                        {
                            // Tell the user to pass an archive path to extract
                            MessageBox.Show(null, "No archive passed for extraction.\nPlease use ZipMan in the following syntax for a successful extraction.\nZipMan.exe --extract {absolute archive path}", "ZipMan | Extraction Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Application.Exit();
                        }
                        break;
                    case "--create":
                        // [TODO]: Start the new archive creating form and pass the required args
                        MessageBox.Show(null, "This functionality is not yet implemented.", "ZipMan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "--preferencess":
                        // {TODO]: Show the preferencess form which will save the settings in a JSON or XML
                        MessageBox.Show(null, "This functionality is not yet implemented.", "ZipMan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    default:
                        // [TODO]: Show an error message box and display the about form
                        MessageBox.Show(null, "Unrecognized arguments were sent.", "ZipMan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }
            }
            else
            {
                // Show the about page
                Application.Run(new AboutWindow());
            }
        }

        #endregion

        #region ControlEvents

        private static void UpdateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Check for any updates, unless the user disabled automatic updates
            if (Properties.Settings.Default.checkForUpdatesOnStart == true)
            {
                // Initialize the new updater
                Update_Manager.Updater.InitalizeUpdater();

                // Read if there is a new update
                if (Update_Manager.Updater.UpdateAvailable)
                {
                    // Pass the gethered parameters from UpdaterClass to the UpdaterWindow
                    DialogResult answer = MessageBox.Show(null, "The new version " + Update_Manager.Updater.NewVersion + " has been released.\nDo you want to update now?", "ZipMan", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (answer == DialogResult.Yes)
                    {
                        Update_Manager.UpdateDownloadWindow NewUpdaterWindow;
                        NewUpdaterWindow = new Update_Manager.UpdateDownloadWindow(Update_Manager.Updater.NewVersion, Update_Manager.Updater.NewDownloadLink);

                        NewUpdaterWindow.ShowDialog();
                    }
                }
            }
        }

        #endregion
    }
}
