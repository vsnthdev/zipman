﻿namespace ZipMan.Update_Manager
{
    partial class UpdateDownloadWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateDownloadWindow));
            this.lblAppName = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnUpdateNow = new System.Windows.Forms.Button();
            this.lblUpgradeInfo = new System.Windows.Forms.Label();
            this.lblDownloadProgress = new System.Windows.Forms.Label();
            this.pbDownloadProgress = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // lblAppName
            // 
            this.lblAppName.AutoSize = true;
            this.lblAppName.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAppName.ForeColor = System.Drawing.Color.Black;
            this.lblAppName.Location = new System.Drawing.Point(12, 9);
            this.lblAppName.Name = "lblAppName";
            this.lblAppName.Size = new System.Drawing.Size(92, 32);
            this.lblAppName.TabIndex = 4;
            this.lblAppName.Text = "ZipMan";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(12, 138);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnUpdateNow
            // 
            this.btnUpdateNow.Enabled = false;
            this.btnUpdateNow.Location = new System.Drawing.Point(435, 138);
            this.btnUpdateNow.Name = "btnUpdateNow";
            this.btnUpdateNow.Size = new System.Drawing.Size(116, 23);
            this.btnUpdateNow.TabIndex = 6;
            this.btnUpdateNow.Text = "Update Now";
            this.btnUpdateNow.UseVisualStyleBackColor = true;
            this.btnUpdateNow.Click += new System.EventHandler(this.btnUpdateNow_Click);
            // 
            // lblUpgradeInfo
            // 
            this.lblUpgradeInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblUpgradeInfo.Location = new System.Drawing.Point(98, 47);
            this.lblUpgradeInfo.Name = "lblUpgradeInfo";
            this.lblUpgradeInfo.Size = new System.Drawing.Size(367, 15);
            this.lblUpgradeInfo.TabIndex = 7;
            this.lblUpgradeInfo.Text = "Upgrading From Version {Version} To Version {Version}.";
            this.lblUpgradeInfo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblDownloadProgress
            // 
            this.lblDownloadProgress.AutoSize = true;
            this.lblDownloadProgress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lblDownloadProgress.Location = new System.Drawing.Point(27, 85);
            this.lblDownloadProgress.Name = "lblDownloadProgress";
            this.lblDownloadProgress.Size = new System.Drawing.Size(213, 15);
            this.lblDownloadProgress.TabIndex = 7;
            this.lblDownloadProgress.Text = "Download Progress: Starting Download";
            // 
            // pbDownloadProgress
            // 
            this.pbDownloadProgress.Location = new System.Drawing.Point(30, 103);
            this.pbDownloadProgress.Name = "pbDownloadProgress";
            this.pbDownloadProgress.Size = new System.Drawing.Size(502, 18);
            this.pbDownloadProgress.TabIndex = 8;
            // 
            // UpdateDownloadWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(563, 173);
            this.Controls.Add(this.pbDownloadProgress);
            this.Controls.Add(this.lblDownloadProgress);
            this.Controls.Add(this.lblUpgradeInfo);
            this.Controls.Add(this.btnUpdateNow);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblAppName);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UpdateDownloadWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ZipMan | Download New Update";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.UpdateDownloadWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label lblAppName;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnUpdateNow;
        private System.Windows.Forms.Label lblUpgradeInfo;
        private System.Windows.Forms.Label lblDownloadProgress;
        private System.Windows.Forms.ProgressBar pbDownloadProgress;
    }
}