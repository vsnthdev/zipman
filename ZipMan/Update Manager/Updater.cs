﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace ZipMan.Update_Manager
{
    class Updater
    {
        // This class checks weather there is a new update available
        // Based on the setting name: checkForUpdatesOnStart

        #region Properties

        // Property to check if there is a new version available
        public static bool UpdateAvailable { set; get; } = false;

        // Property to get the new version download link according to CPU architecture
        public static string NewDownloadLink { set; get; } = string.Empty;

        // Property to get the new version string
        public static string NewVersion { set; get; } = string.Empty;

        // Property to check if the update check was successful
        public static bool HitToServer { set; get; } = false;

        #endregion

        #region Initialization

        public static void InitalizeUpdater()
        {
            // Run this method before reading any values

            // Download the updater.json from GitHub repository
            try
            {
                string jsonString = new WebClient().DownloadString("https://raw.githubusercontent.com/vasanthdeveloper/ZipMan/updater/updater.json");

                HitToServer = true;

                // Parse the JSON string into a dynamic object
                dynamic versionData = JsonConvert.DeserializeObject(jsonString);

                // Get the first entry
                dynamic latestVersionData = versionData[0];

                // Varialbes for current version and latest version
                int currentVersion = Convert.ToInt32(appSettings.AppVersionManager.Major.ToString() + appSettings.AppVersionManager.Minor.ToString());
                int latestVersion = Convert.ToInt32(latestVersionData["major"].ToString() + latestVersionData["minor"].ToString());

                // Check if there is a new version
                if (latestVersion > currentVersion)
                {
                    // Set UpdateAvailable to true
                    UpdateAvailable = true;

                    // Construct the version string of latest release and set it as the property
                    NewVersion = latestVersionData["major"].ToString() + "." + latestVersionData["minor"].ToString();

                    // Check if the operating system is a 64-bit version
                    if (Environment.Is64BitOperatingSystem)
                    {
                        // Set the 64-bit download link
                        NewDownloadLink = latestVersionData["dl_64"].ToString();
                    }
                    else
                    {
                        // Set the 32-bit download link
                        NewDownloadLink = latestVersionData["dl_86"].ToString();
                    }
                }
            } catch
            {
                // [TODO]: Log the error into the log file
                HitToServer = false;
            }

            
        }

        #endregion
    }
}
