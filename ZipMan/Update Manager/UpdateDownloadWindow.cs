﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace ZipMan.Update_Manager
{
    public partial class UpdateDownloadWindow : Form
    {
        #region Properties

        private string newVersionDownloadLink;

        #endregion

        #region Initialization

        public UpdateDownloadWindow(string newVerString, string newDLLink)
        {
            InitializeComponent();
            newVersionDownloadLink = newDLLink;

            // Set the latest version value
            lblUpgradeInfo.Text = "Upgrading From Version " + appSettings.AppVersion + " To Version " + newVerString + ".";
        }

        private void UpdateDownloadWindow_Load(object sender, EventArgs e)
        {
            using (WebClient wc = new WebClient())
            {
                wc.DownloadProgressChanged += Wc_DownloadProgressChanged;
                wc.DownloadFileCompleted += Wc_DownloadFileCompleted;
                Uri DownloadAddress = new Uri(newVersionDownloadLink, UriKind.Absolute);
                wc.DownloadFileAsync(DownloadAddress, Path.Combine(appSettings.appInstallLocation, "update.exe"));
            }
        }

        #endregion

        #region ControlEvents

        private void Wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            pbDownloadProgress.Value = e.ProgressPercentage;
            lblDownloadProgress.Text = "Download Progress: " + e.ProgressPercentage + "%";
        }

        private void Wc_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            lblDownloadProgress.Text = "Download Progress: Download Finished!";
            btnUpdateNow.Enabled = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                File.Delete(Path.Combine(appSettings.appInstallLocation, "update.exe"));
            }
            catch { }
            Dispose();
        }

        private void btnUpdateNow_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(Path.Combine(appSettings.appInstallLocation, "update.exe"));
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Unable to launch the downloaded installer for the new version.\nUpdate aborted due to:\n" + ex.Message.ToString() + "\nPlease manually open the installer after exiting ZipMan.", "ZipMan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

    }
}
