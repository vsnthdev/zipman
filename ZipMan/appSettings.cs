﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ZipMan
{
    class appSettings
    {
        // [INFO] This is the class that holds different settings which can be accessed throughout the application

        #region VersionManagement
        // These are the values for Application Version
        public static string AppLabel { get; } = "(Beta)";
        public static Version AppVersionManager = Assembly.GetExecutingAssembly().GetName().Version;
        public static string AppVersion { get; } = AppVersionManager.Major.ToString() + "." + AppVersionManager.Minor.ToString();
        #endregion

        // For getting application exe path
        public static string appInstallLocation { get; } = AppDomain.CurrentDomain.BaseDirectory;
    }
}
