﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZipMan
{
    public partial class AboutWindow : Form
    {
        #region Initialization

        public AboutWindow()
        {
            InitializeComponent();

            // Set the version label
            lblVersion.Text = "Version: " + appSettings.AppVersion;
            if (Environment.Is64BitProcess == true)
            {
                lblRelease.Text = "Release: " + appSettings.AppVersion + " 64-bit " + appSettings.AppLabel;
            } else
            {
                lblRelease.Text = "Release: " + appSettings.AppVersion + " 32-bit " + appSettings.AppLabel;
            }

            // Allow our update background worker to access the form
            CheckForIllegalCrossThreadCalls = false;
        }

        #endregion

        #region ControlEvents

        private void btnDonate_Click(object sender, EventArgs e)
        {
            tabcAbout.SelectedIndex = 1;
        }

        private void lblWebsiteLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://zipman.vasanthdeveloper.com");
        }

        private void btnPayPalDonate_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.paypal.me/vasanthdeveloper");
        }

        private void btnCheckForUpdates_Click(object sender, EventArgs e)
        {
            // Set the button text to checking updates
            btnCheckForUpdates.Text = "Checking...";

            // Disable the button to prevent future clicks
            btnCheckForUpdates.Enabled = false;

            // Initialize a new background work for checking updates in background
            BackgroundWorker UpdateWorker;
            UpdateWorker = new BackgroundWorker();
            UpdateWorker.DoWork += UpdateWorker_DoWork;
            UpdateWorker.RunWorkerAsync();
        }

        private void UpdateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Update_Manager.Updater.InitalizeUpdater();

            // Check if the update failed
            if (Update_Manager.Updater.HitToServer == false)
            {
                MessageBox.Show(this, "Failed to check for updates.", "ZipMan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                // Set back the button text as normal and enable it
                btnCheckForUpdates.Text = "Check for updates";
                btnCheckForUpdates.Enabled = true;
                return;
            }

            if (Update_Manager.Updater.UpdateAvailable)
            {
                // Pass the gethered parameters from UpdaterClass to the UpdaterWindow
                DialogResult answer = MessageBox.Show(this, "The new version " + Update_Manager.Updater.NewVersion + " has been released.\nDo you want to update now?", "ZipMan", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (answer == DialogResult.Yes)
                {
                    Update_Manager.UpdateDownloadWindow NewUpdaterWindow;
                    NewUpdaterWindow = new Update_Manager.UpdateDownloadWindow(Update_Manager.Updater.NewVersion, Update_Manager.Updater.NewDownloadLink);
                    NewUpdaterWindow.ShowDialog();
                }
            } else
            {
                // Tell the user that he has the latest version
                MessageBox.Show(this, "You have the latest version.", "ZipMan", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            // Set back the button text as normal and enable it
            btnCheckForUpdates.Text = "Check for updates";
            btnCheckForUpdates.Enabled = true;
        }

        #endregion
    }
}
