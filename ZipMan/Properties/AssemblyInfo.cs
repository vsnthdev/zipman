﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ZipMan")]
[assembly: AssemblyDescription("Simply double click to extract!")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Vasanth Developer")]
[assembly: AssemblyProduct("ZipMan")]
[assembly: AssemblyCopyright("Copyright © Vasanth Developer 2018")]
[assembly: AssemblyTrademark("Copyright © Vasanth Developer. All Rights Reserved.")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ab8b9bc2-a859-476d-9e22-f961c9fb8fa6")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("18.11.0.0")]
[assembly: AssemblyFileVersion("18.11.0.0")]
