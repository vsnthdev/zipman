﻿namespace ZipMan.Extraction
{
    partial class ExtractionUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExtractionUI));
            this.lblAppLabel = new System.Windows.Forms.Label();
            this.lblInputFile = new System.Windows.Forms.Label();
            this.pbCurrentFileProgress = new System.Windows.Forms.ProgressBar();
            this.lblCurrentFile = new System.Windows.Forms.Label();
            this.pbExtractionProgress = new System.Windows.Forms.ProgressBar();
            this.lblDestinationFolder = new System.Windows.Forms.Label();
            this.cmbAfterCompletionActions = new System.Windows.Forms.ComboBox();
            this.lblAfterCompleting = new System.Windows.Forms.Label();
            this.chkNotifyAfterCompletion = new System.Windows.Forms.CheckBox();
            this.btnRunInBackground = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.lblCopyrights = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblAppLabel
            // 
            this.lblAppLabel.AutoSize = true;
            this.lblAppLabel.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAppLabel.ForeColor = System.Drawing.Color.Black;
            this.lblAppLabel.Location = new System.Drawing.Point(12, 9);
            this.lblAppLabel.Name = "lblAppLabel";
            this.lblAppLabel.Size = new System.Drawing.Size(92, 32);
            this.lblAppLabel.TabIndex = 4;
            this.lblAppLabel.Text = "ZipMan";
            // 
            // lblInputFile
            // 
            this.lblInputFile.AutoSize = true;
            this.lblInputFile.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInputFile.Location = new System.Drawing.Point(31, 53);
            this.lblInputFile.Name = "lblInputFile";
            this.lblInputFile.Size = new System.Drawing.Size(62, 15);
            this.lblInputFile.TabIndex = 6;
            this.lblInputFile.Text = "Input File: ";
            // 
            // pbCurrentFileProgress
            // 
            this.pbCurrentFileProgress.Location = new System.Drawing.Point(34, 96);
            this.pbCurrentFileProgress.Name = "pbCurrentFileProgress";
            this.pbCurrentFileProgress.Size = new System.Drawing.Size(574, 18);
            this.pbCurrentFileProgress.TabIndex = 9;
            // 
            // lblCurrentFile
            // 
            this.lblCurrentFile.AutoSize = true;
            this.lblCurrentFile.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentFile.Location = new System.Drawing.Point(31, 78);
            this.lblCurrentFile.Name = "lblCurrentFile";
            this.lblCurrentFile.Size = new System.Drawing.Size(103, 15);
            this.lblCurrentFile.TabIndex = 8;
            this.lblCurrentFile.Text = "Current File Name";
            // 
            // pbExtractionProgress
            // 
            this.pbExtractionProgress.Location = new System.Drawing.Point(34, 145);
            this.pbExtractionProgress.Name = "pbExtractionProgress";
            this.pbExtractionProgress.Size = new System.Drawing.Size(574, 18);
            this.pbExtractionProgress.TabIndex = 11;
            // 
            // lblDestinationFolder
            // 
            this.lblDestinationFolder.AutoSize = true;
            this.lblDestinationFolder.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDestinationFolder.Location = new System.Drawing.Point(31, 127);
            this.lblDestinationFolder.Name = "lblDestinationFolder";
            this.lblDestinationFolder.Size = new System.Drawing.Size(103, 15);
            this.lblDestinationFolder.TabIndex = 10;
            this.lblDestinationFolder.Text = "Destination Folder";
            // 
            // cmbAfterCompletionActions
            // 
            this.cmbAfterCompletionActions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAfterCompletionActions.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAfterCompletionActions.FormattingEnabled = true;
            this.cmbAfterCompletionActions.Items.AddRange(new object[] {
            "Do Nothing",
            "Shut Down",
            "Reboot",
            "Logoff"});
            this.cmbAfterCompletionActions.Location = new System.Drawing.Point(138, 185);
            this.cmbAfterCompletionActions.Name = "cmbAfterCompletionActions";
            this.cmbAfterCompletionActions.Size = new System.Drawing.Size(121, 23);
            this.cmbAfterCompletionActions.TabIndex = 13;
            // 
            // lblAfterCompleting
            // 
            this.lblAfterCompleting.AutoSize = true;
            this.lblAfterCompleting.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAfterCompleting.Location = new System.Drawing.Point(34, 188);
            this.lblAfterCompleting.Name = "lblAfterCompleting";
            this.lblAfterCompleting.Size = new System.Drawing.Size(102, 15);
            this.lblAfterCompleting.TabIndex = 12;
            this.lblAfterCompleting.Text = "After Completion:";
            // 
            // chkNotifyAfterCompletion
            // 
            this.chkNotifyAfterCompletion.AutoSize = true;
            this.chkNotifyAfterCompletion.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkNotifyAfterCompletion.Location = new System.Drawing.Point(304, 237);
            this.chkNotifyAfterCompletion.Name = "chkNotifyAfterCompletion";
            this.chkNotifyAfterCompletion.Size = new System.Drawing.Size(154, 19);
            this.chkNotifyAfterCompletion.TabIndex = 16;
            this.chkNotifyAfterCompletion.Text = "Notify After Completion";
            this.chkNotifyAfterCompletion.UseVisualStyleBackColor = true;
            // 
            // btnRunInBackground
            // 
            this.btnRunInBackground.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRunInBackground.ForeColor = System.Drawing.Color.Black;
            this.btnRunInBackground.Location = new System.Drawing.Point(464, 233);
            this.btnRunInBackground.Name = "btnRunInBackground";
            this.btnRunInBackground.Size = new System.Drawing.Size(144, 23);
            this.btnRunInBackground.TabIndex = 14;
            this.btnRunInBackground.Text = "Run In Background";
            this.btnRunInBackground.UseVisualStyleBackColor = true;
            // 
            // btnStop
            // 
            this.btnStop.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.ForeColor = System.Drawing.Color.Black;
            this.btnStop.Location = new System.Drawing.Point(34, 234);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 15;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            // 
            // lblCopyrights
            // 
            this.lblCopyrights.AutoSize = true;
            this.lblCopyrights.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopyrights.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(176)))), ((int)(((byte)(190)))), ((int)(((byte)(197)))));
            this.lblCopyrights.Location = new System.Drawing.Point(12, 275);
            this.lblCopyrights.Name = "lblCopyrights";
            this.lblCopyrights.Size = new System.Drawing.Size(283, 15);
            this.lblCopyrights.TabIndex = 17;
            this.lblCopyrights.Text = "Copyright © Vasanth Developer. All Rights Reserved.";
            // 
            // lblVersion
            // 
            this.lblVersion.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(125)))), ((int)(((byte)(139)))));
            this.lblVersion.Location = new System.Drawing.Point(504, 275);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(118, 15);
            this.lblVersion.TabIndex = 18;
            this.lblVersion.Text = "Version:";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ExtractionUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(634, 299);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.lblCopyrights);
            this.Controls.Add(this.chkNotifyAfterCompletion);
            this.Controls.Add(this.btnRunInBackground);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.cmbAfterCompletionActions);
            this.Controls.Add(this.lblAfterCompleting);
            this.Controls.Add(this.pbExtractionProgress);
            this.Controls.Add(this.lblDestinationFolder);
            this.Controls.Add(this.pbCurrentFileProgress);
            this.Controls.Add(this.lblCurrentFile);
            this.Controls.Add(this.lblInputFile);
            this.Controls.Add(this.lblAppLabel);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ExtractionUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ZipMan";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label lblAppLabel;
        internal System.Windows.Forms.Label lblInputFile;
        internal System.Windows.Forms.ProgressBar pbCurrentFileProgress;
        internal System.Windows.Forms.Label lblCurrentFile;
        internal System.Windows.Forms.ProgressBar pbExtractionProgress;
        internal System.Windows.Forms.Label lblDestinationFolder;
        internal System.Windows.Forms.ComboBox cmbAfterCompletionActions;
        internal System.Windows.Forms.Label lblAfterCompleting;
        internal System.Windows.Forms.CheckBox chkNotifyAfterCompletion;
        internal System.Windows.Forms.Button btnRunInBackground;
        internal System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label lblCopyrights;
        private System.Windows.Forms.Label lblVersion;
    }
}