﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZipMan.Extraction
{
    public partial class ExtractionUI : Form
    {
        #region ExtractionVariables

        private string ArchivePath;

        #endregion

        #region Initialization

        public ExtractionUI(string archivePath)
        {
            InitializeComponent();

            // Pass the archive path to the global variable, so that it can be used in the Load Event
            ArchivePath = archivePath;

            // Set the version label
            lblVersion.Text = "Version: " + appSettings.AppVersion;
        }

        #endregion
    }
}
