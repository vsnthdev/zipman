﻿namespace ZipMan
{
    partial class AboutWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutWindow));
            this.lblCopyrights = new System.Windows.Forms.Label();
            this.picAppIcon = new System.Windows.Forms.PictureBox();
            this.lblAppName = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.tabcAbout = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lblRelease = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblWebsiteLink = new System.Windows.Forms.LinkLabel();
            this.btnDonate = new System.Windows.Forms.Button();
            this.btnCheckForUpdates = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lblDonationText = new System.Windows.Forms.Label();
            this.btnPayPalDonate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picAppIcon)).BeginInit();
            this.tabcAbout.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCopyrights
            // 
            this.lblCopyrights.AutoSize = true;
            this.lblCopyrights.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(176)))), ((int)(((byte)(190)))), ((int)(((byte)(197)))));
            this.lblCopyrights.Location = new System.Drawing.Point(12, 488);
            this.lblCopyrights.Name = "lblCopyrights";
            this.lblCopyrights.Size = new System.Drawing.Size(283, 15);
            this.lblCopyrights.TabIndex = 2;
            this.lblCopyrights.Text = "Copyright © Vasanth Developer. All Rights Reserved.";
            // 
            // picAppIcon
            // 
            this.picAppIcon.Image = global::ZipMan.Properties.Resources.AppIcon2x;
            this.picAppIcon.Location = new System.Drawing.Point(124, 21);
            this.picAppIcon.Name = "picAppIcon";
            this.picAppIcon.Size = new System.Drawing.Size(214, 143);
            this.picAppIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picAppIcon.TabIndex = 0;
            this.picAppIcon.TabStop = false;
            // 
            // lblAppName
            // 
            this.lblAppName.Font = new System.Drawing.Font("Segoe UI Light", 18F);
            this.lblAppName.ForeColor = System.Drawing.Color.Black;
            this.lblAppName.Location = new System.Drawing.Point(161, 173);
            this.lblAppName.Name = "lblAppName";
            this.lblAppName.Size = new System.Drawing.Size(141, 37);
            this.lblAppName.TabIndex = 1;
            this.lblAppName.Text = "ZipMan";
            this.lblAppName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblVersion
            // 
            this.lblVersion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(125)))), ((int)(((byte)(139)))));
            this.lblVersion.Location = new System.Drawing.Point(332, 488);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(118, 15);
            this.lblVersion.TabIndex = 3;
            this.lblVersion.Text = "Version:";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabcAbout
            // 
            this.tabcAbout.Controls.Add(this.tabPage1);
            this.tabcAbout.Controls.Add(this.tabPage2);
            this.tabcAbout.Location = new System.Drawing.Point(26, 228);
            this.tabcAbout.Name = "tabcAbout";
            this.tabcAbout.SelectedIndex = 0;
            this.tabcAbout.Size = new System.Drawing.Size(411, 243);
            this.tabcAbout.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lblRelease);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.lblWebsiteLink);
            this.tabPage1.Controls.Add(this.btnDonate);
            this.tabPage1.Controls.Add(this.btnCheckForUpdates);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(403, 215);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Information";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lblRelease
            // 
            this.lblRelease.AutoSize = true;
            this.lblRelease.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblRelease.Location = new System.Drawing.Point(15, 63);
            this.lblRelease.Name = "lblRelease";
            this.lblRelease.Size = new System.Drawing.Size(57, 19);
            this.lblRelease.TabIndex = 4;
            this.lblRelease.Text = "Release:";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.label1.Location = new System.Drawing.Point(16, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(370, 46);
            this.label1.TabIndex = 3;
            this.label1.Text = "Thanks to Paomedia from Iconfinder.com for giving me an icon.\r\nFor any suggestion" +
    "s, feature requests contact vasanthdevelopers@gmail.com.";
            // 
            // lblWebsiteLink
            // 
            this.lblWebsiteLink.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(136)))), ((int)(((byte)(209)))));
            this.lblWebsiteLink.AutoSize = true;
            this.lblWebsiteLink.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(255)))));
            this.lblWebsiteLink.Location = new System.Drawing.Point(8, 194);
            this.lblWebsiteLink.Name = "lblWebsiteLink";
            this.lblWebsiteLink.Size = new System.Drawing.Size(74, 15);
            this.lblWebsiteLink.TabIndex = 2;
            this.lblWebsiteLink.TabStop = true;
            this.lblWebsiteLink.Text = "Visit Website";
            this.lblWebsiteLink.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(39)))), ((int)(((byte)(176)))));
            this.lblWebsiteLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblWebsiteLink_LinkClicked);
            // 
            // btnDonate
            // 
            this.btnDonate.Location = new System.Drawing.Point(188, 186);
            this.btnDonate.Name = "btnDonate";
            this.btnDonate.Size = new System.Drawing.Size(75, 23);
            this.btnDonate.TabIndex = 1;
            this.btnDonate.Text = "Donate";
            this.btnDonate.UseVisualStyleBackColor = true;
            this.btnDonate.Click += new System.EventHandler(this.btnDonate_Click);
            // 
            // btnCheckForUpdates
            // 
            this.btnCheckForUpdates.Location = new System.Drawing.Point(269, 186);
            this.btnCheckForUpdates.Name = "btnCheckForUpdates";
            this.btnCheckForUpdates.Size = new System.Drawing.Size(128, 23);
            this.btnCheckForUpdates.TabIndex = 0;
            this.btnCheckForUpdates.Text = "Check for updates";
            this.btnCheckForUpdates.UseVisualStyleBackColor = true;
            this.btnCheckForUpdates.Click += new System.EventHandler(this.btnCheckForUpdates_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lblDonationText);
            this.tabPage2.Controls.Add(this.btnPayPalDonate);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(403, 215);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Support ZipMan";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lblDonationText
            // 
            this.lblDonationText.BackColor = System.Drawing.Color.Transparent;
            this.lblDonationText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.lblDonationText.Location = new System.Drawing.Point(16, 14);
            this.lblDonationText.Name = "lblDonationText";
            this.lblDonationText.Size = new System.Drawing.Size(370, 154);
            this.lblDonationText.TabIndex = 1;
            this.lblDonationText.Text = resources.GetString("lblDonationText.Text");
            // 
            // btnPayPalDonate
            // 
            this.btnPayPalDonate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.btnPayPalDonate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPayPalDonate.ForeColor = System.Drawing.Color.White;
            this.btnPayPalDonate.Location = new System.Drawing.Point(55, 177);
            this.btnPayPalDonate.Name = "btnPayPalDonate";
            this.btnPayPalDonate.Size = new System.Drawing.Size(293, 28);
            this.btnPayPalDonate.TabIndex = 0;
            this.btnPayPalDonate.Text = "Donate On PayPal";
            this.btnPayPalDonate.UseVisualStyleBackColor = false;
            this.btnPayPalDonate.Click += new System.EventHandler(this.btnPayPalDonate_Click);
            // 
            // AboutWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(462, 512);
            this.Controls.Add(this.tabcAbout);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.lblCopyrights);
            this.Controls.Add(this.lblAppName);
            this.Controls.Add(this.picAppIcon);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AboutWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ZipMan";
            ((System.ComponentModel.ISupportInitialize)(this.picAppIcon)).EndInit();
            this.tabcAbout.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCopyrights;
        private System.Windows.Forms.PictureBox picAppIcon;
        private System.Windows.Forms.Label lblAppName;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.TabControl tabcAbout;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnCheckForUpdates;
        private System.Windows.Forms.Button btnDonate;
        private System.Windows.Forms.LinkLabel lblWebsiteLink;
        private System.Windows.Forms.Button btnPayPalDonate;
        private System.Windows.Forms.Label lblDonationText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRelease;
    }
}